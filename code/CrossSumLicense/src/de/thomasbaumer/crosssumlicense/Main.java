package de.thomasbaumer.crosssumlicense;

import java.util.ArrayList;
import java.util.Random;

/**
 * Calculates a String with a cross sum of CROSS_SUM_BOUNDARY (920).
 */
public class Main {
    //smallest ascii character "!" which will be part of the license
    private static final int ASCII_MIN = 33;
    //biggest ascii character "~" which will be part of the license
    private static final int ASCII_MAX = 126;
    //desired cross sum value of the license
    private static final int CROSS_SUM_BOUNDARY = 920;

    /**
     * Prints a valid license for the lizenz3 program.
     * @param args not required, nor used
     */
    public static void main(String[] args) {
        String license = null;
        while (license == null) {
            //get the license, will be "null" if the heuristic algorithm returns a invalid license
            license = reduceToBoundary(getRandomWordOverBoundary());
        }
        System.out.println("A valid license is " + license);
        System.out.println("Some characters may need to be escaped!");
    }

    /**
     * Expects a String word which has a cross sum over CROSS_SUM_BOUNDARY
     * and returns a word with reduced cross sum to CROSS_SUM_BOUNDARY.
     * @param word is a String above the cross sum value CROSS_SUM_BOUNDARY.
     * @return a String with the cross sum value CROSS_SUM_BOUNDARY (or "null" if the heuristic algorithm failed).
     */
    private static String reduceToBoundary(String word) {
        //removes the last two characters of the random word over the cross sum CROSS_SUM_BOUNDARY
        String smallWord = word.substring(0, word.length() - 2);
        int crossSum = calculateCrossSum(smallWord);
        int diff = (crossSum - Main.CROSS_SUM_BOUNDARY)*(-1);

        //refills the last to characters with a mean value of the required diff to match CROSS_SUM_BOUNDARY
        if (diff/2 >= ASCII_MIN) return smallWord + (char) (diff / 2) + (char) (diff - diff / 2);
        //this heuristic may fail. If that's the case "null" will be returned.
        System.out.println("Failed attempt, try again...");
        return null;
    }

    /**
     * Generates a random String which got a bigger cross sum than CROSS_SUM_BOUNDARY.
     * @return a random String which got a bigger cross sum than CROSS_SUM_BOUNDARY
     */
    private static String getRandomWordOverBoundary() {
        ArrayList<Character> randomChars = new ArrayList<>();
        Random random = new Random();
        //add a character until CROSS_SUM_BOUNDARY is reached
        while (calculateCrossSum(convertListToString(randomChars)) < Main.CROSS_SUM_BOUNDARY) {
            //get a random and valid character
            char randomChar = (char) (random.nextInt(ASCII_MAX - ASCII_MIN) + ASCII_MIN);
            randomChars.add(randomChar);
        }
        //return the list converted to String
        return convertListToString(randomChars);
    }

    /**
     * Convert character list to String
     * @param list an ArrayList<Character> with random characters
     * @return return a String
     */
    private static String convertListToString(ArrayList<Character> list) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Character c : list) stringBuilder.append(c);
        return stringBuilder.toString();
    }

    /**
     * Calculate a cross sum of a given String.
     * @param word which is the basis for the cross sum calculation
     * @return the cross sum of the String word.
     */
    private static int calculateCrossSum(String word) {
        int crossSum = 0;
        for (char c : word.toCharArray()) crossSum += c;
        return crossSum;
    }
}
